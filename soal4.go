//Created by Rehan - 081295955149
package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

func runesToString(runes []rune) (outString string) {
	// don't need index so _
	for _, v := range runes {
		outString += string(v)
	}
	return
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("---------------------")
	fmt.Print("T-> ")
	input1, _ := reader.ReadString('\n')
	for i := 0; i < len(input1)-1; i++ {
		var simpan string
		var inputA, inputB string
		var sA, sB []rune
		fmt.Println("---------------------")
		fmt.Print("A-> ")
		inputA, _ = reader.ReadString('\n')
		fmt.Print("B-> ")
		inputB, _ = reader.ReadString('\n')
		//proses
		sA = []rune(inputA)
		sB = []rune(inputB)

		re_leadclose_whtsp := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
		re_inside_whtsp := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
		for len(sA) != 0 || len(sB) != 0 {
			if string(sA[0]) == string(sB[0]) {
				simpan += string(sB[0])
				inputB = inputB[1:]
				//fmt.Println(inputB)
				sBBB := re_leadclose_whtsp.ReplaceAllString(inputB, "")
				sBBB = re_inside_whtsp.ReplaceAllString(sBBB, " ")
				//fmt.Println(sBBB)
				inputB = sBBB
				sB = []rune(inputB)

			} else {
				//fmt.Println(string(sA[0]) + " < " + string(sB[0]))
				if string(sA[0]) < string(sB[0]) {
					simpan += string(sA[0])
					inputA = inputA[1:]
					//fmt.Println(inputA)
					sAAA := re_leadclose_whtsp.ReplaceAllString(inputA, "")
					sAAA = re_inside_whtsp.ReplaceAllString(sAAA, " ")
					//fmt.Println(sAAA)
					inputA = sAAA
					sA = []rune(inputA)
				} else {
					simpan += string(sB[0])
					inputB = inputB[1:]
					//fmt.Println(inputB)
					sBBB := re_leadclose_whtsp.ReplaceAllString(inputB, "")
					sBBB = re_inside_whtsp.ReplaceAllString(sBBB, " ")
					//fmt.Println(sBBB)
					inputB = sBBB
					sB = []rune(inputB)

				}
			}
			//fmt.Println("Output => ", simpan)
			if len(inputA) == 0 || len(inputB) == 0 {
				if len(inputA) != 0 {
					simpan += inputA
				}
				if len(inputB) != 0 {
					simpan += inputB
				}

				break
			}

		}
		fmt.Println("---------------------")
		fmt.Println("Output => ", simpan)

	}

}
